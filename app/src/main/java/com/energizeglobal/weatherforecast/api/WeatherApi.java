package com.energizeglobal.weatherforecast.api;

import com.energizeglobal.weatherforecast.BuildConfig;
import com.energizeglobal.weatherforecast.model.CurrentWeatherResponse;
import com.energizeglobal.weatherforecast.model.DailyForecastResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {

    @GET("forecasts/"+BuildConfig.API_VERSION_CODE + "/daily/5day/2159713")
    Observable<DailyForecastResponse> getDailyForecast(@Query("details") boolean details);

    @GET("currentconditions/"+BuildConfig.API_VERSION_CODE + "/2159713")
    Observable<List<CurrentWeatherResponse>> getCurrentWeather(@Query("details") boolean details);

}
