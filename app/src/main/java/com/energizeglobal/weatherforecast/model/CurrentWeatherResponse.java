package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CurrentWeatherResponse extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("CloudCover")
    private int mCloud;

    @SerializedName("WeatherIcon")
    private int mWeatherIcon;

    @SerializedName("Pressure")
    private MetricItem mPressure;

    @SerializedName("RealFeelTemperature")
    private MetricItem mTemperature;

    @SerializedName("Wind")
    private WindItem mWind;

    @SerializedName("WeatherText")
    private String mWeatherText;

    @SerializedName("RelativeHumidity")
    private double mHumidity;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public int getCloud() {
        return mCloud;
    }

    public void setCloud(int cloud) {
        mCloud = cloud;
    }

    public MetricItem getPressure() {
        return mPressure;
    }

    public void setPressure(MetricItem pressure) {
        mPressure = pressure;
    }

    public MetricItem getTemperature() {
        return mTemperature;
    }

    public void setTemperature(MetricItem temperature) {
        mTemperature = temperature;
    }

    public WindItem getWind() {
        return mWind;
    }

    public void setWind(WindItem wind) {
        mWind = wind;
    }

    public String getWeatherText() {
        return mWeatherText;
    }

    public void setWeatherText(String weatherText) {
        mWeatherText = weatherText;
    }

    public double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(double humidity) {
        mHumidity = humidity;
    }

    public int getWeatherIcon() {
        return mWeatherIcon;
    }

    public void setWeatherIcon(int weatherIcon) {
        mWeatherIcon = weatherIcon;
    }

    @Override
    public int hashCode() {
        return (int) (mHumidity+mPressure.getMetric().getValue()+mCloud);
    }
}
