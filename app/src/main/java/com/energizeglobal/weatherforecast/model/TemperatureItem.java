package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class TemperatureItem extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("Minimum")
    private ValueItem mMin;

    @SerializedName("Maximum")
    private ValueItem mMax;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public ValueItem getMin() {
        return mMin;
    }

    public void setMin(ValueItem min) {
        mMin = min;
    }

    public ValueItem getMax() {
        return mMax;
    }

    public void setMax(ValueItem max) {
        mMax = max;
    }

    @Override
    public int hashCode() {
        mId = mMax.hashCode()+mMin.hashCode();
        return (int) mId;
    }
}