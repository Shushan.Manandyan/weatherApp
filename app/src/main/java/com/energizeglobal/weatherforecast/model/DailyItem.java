package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class DailyItem extends RealmObject {

    @PrimaryKey
    @SerializedName("EpochDate")
    public long mDate;

    @SerializedName("Sun")
    public SunItem mSun;

    @SerializedName("Temperature")
    public TemperatureItem mTemperature;


    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public SunItem getSun() {
        return mSun;
    }

    public void setSun(SunItem sun) {
        mSun = sun;
    }

    public TemperatureItem getTemperature() {
        return mTemperature;
    }

    public void setTemperature(TemperatureItem temperature) {
        mTemperature = temperature;
    }
}