package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class WindItem extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("Speed")
    private MetricItem mSpeed;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public MetricItem getSpeed() {
        return mSpeed;
    }

    public void setSpeed(MetricItem speed) {
        mSpeed = speed;
    }
}