package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class MetricItem extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("Metric")
    private ValueItem mMetric;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public ValueItem getMetric() {
        return mMetric;
    }

    public void setMetric(ValueItem metric) {
        mMetric = metric;
    }

    @Override
    public int hashCode() {
        mId = (long) mMetric.hashCode();
        return mMetric.hashCode();
    }
}
