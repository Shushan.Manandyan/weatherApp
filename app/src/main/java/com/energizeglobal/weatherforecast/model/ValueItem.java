package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ValueItem extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("Value")
    private double mValue;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public double getValue() {
        return mValue;
    }

    public void setValue(double value) {
        mValue = value;
    }

    @Override
    public int hashCode() {
        mId = (long) mValue;
        return (int) (mValue);
    }
}
