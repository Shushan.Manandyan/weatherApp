package com.energizeglobal.weatherforecast.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class DailyForecastResponse extends RealmObject {

    @PrimaryKey
    private long mId;

    @SerializedName("DailyForecasts")
    private RealmList<DailyItem> mDailyItems;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public RealmList<DailyItem> getDailyItems() {
        return mDailyItems;
    }

    public void setDailyItems(RealmList<DailyItem> dailyItems) {
        mDailyItems = dailyItems;
    }

}
