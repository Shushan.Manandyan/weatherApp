package com.energizeglobal.weatherforecast.viewModel;

import com.energizeglobal.weatherforecast.model.CurrentWeatherResponse;
import com.energizeglobal.weatherforecast.model.DailyForecastResponse;

public interface MainActivityView extends IView {

    void loadDailyForecast(DailyForecastResponse items);

    void loadCurrentWeather(CurrentWeatherResponse items);
}
