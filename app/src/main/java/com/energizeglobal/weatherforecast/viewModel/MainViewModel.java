package com.energizeglobal.weatherforecast.viewModel;

import com.energizeglobal.weatherforecast.api.WeatherApi;
import com.energizeglobal.weatherforecast.model.CurrentWeatherResponse;
import com.energizeglobal.weatherforecast.model.DailyForecastResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class MainViewModel extends BaseViewModel<MainActivityView> {

    private WeatherApi mWeatherApi;

    public MainViewModel(WeatherApi weatherApi) {
        mWeatherApi = weatherApi;
    }

    public void loadDailyForecast() {
        mCompositeDisposable.add(mWeatherApi.getDailyForecast(true)
                .subscribeOn(Schedulers.computation())
                .subscribe(dailyForecastResponse -> {
                            if (dailyForecastResponse != null && dailyForecastResponse.getDailyItems() != null) {
                                try (Realm r = Realm.getDefaultInstance()) {
                                    r.executeTransaction((realm) -> {
                                        realm.delete(DailyForecastResponse.class);
                                        realm.insertOrUpdate(dailyForecastResponse);
                                    });
                                } catch (Exception e) {
                                    mView.error(new Throwable());
                                }
                            }
                        },
                        throwable -> {
                            mView.error(new Throwable());
                        }));
    }

    public void loadCurrentWeatherData() {
        mCompositeDisposable.add(mWeatherApi.getCurrentWeather(true)
                .subscribeOn(Schedulers.computation())
                .subscribe(currentWeatherResponse -> {
                            if (currentWeatherResponse != null && currentWeatherResponse.size() > 0) {
                                try (Realm r = Realm.getDefaultInstance()) {
                                    r.executeTransaction((realm) -> {
                                        realm.delete(CurrentWeatherResponse.class);
                                        realm.insertOrUpdate(currentWeatherResponse);
                                    });
                                } catch (Exception e) {
                                    mView.error(new Throwable());
                                }
                            }
                        },
                        throwable -> {
                            mView.error(new Throwable());
                        }));
    }

    public void getWeatherData() {
        loadCurrentWeatherData();
        loadDailyForecast();

        mCompositeDisposable.add(Realm.getDefaultInstance()
                .where(CurrentWeatherResponse.class)
                .findAllAsync().asFlowable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(currentWeatherResponse -> {
                    if (currentWeatherResponse != null && currentWeatherResponse.size() > 0) {
                        mView.loadCurrentWeather(currentWeatherResponse.get(0));
                    }
                }));

        mCompositeDisposable.add(Realm.getDefaultInstance()
                .where(DailyForecastResponse.class)
                .findAllAsync().asFlowable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dailyForecastResponse -> {
                    if (dailyForecastResponse != null && dailyForecastResponse.size() > 0) {
                        mView.loadDailyForecast(dailyForecastResponse.get(0));
                    }
                }));
    }
}
