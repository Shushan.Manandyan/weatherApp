package com.energizeglobal.weatherforecast.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.energizeglobal.weatherforecast.R;
import com.energizeglobal.weatherforecast.databinding.DailyRowLayoutBinding;
import com.energizeglobal.weatherforecast.model.DailyItem;

import io.realm.RealmList;

public class DailyForecastAdapter extends RecyclerView.Adapter<DailyForecastAdapter.TrendsViewHolder> {

    private RealmList<DailyItem> mDailyItems = new RealmList<>();

    @Override
    public TrendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DailyRowLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.daily_row_layout, parent, false);
        return new TrendsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(TrendsViewHolder holder, int position) {
        holder.update(mDailyItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mDailyItems.size();
    }


    public void setTempItems( RealmList<DailyItem> dailyItems) {
        mDailyItems = dailyItems;
        notifyDataSetChanged();
    }

    public static class TrendsViewHolder extends RecyclerView.ViewHolder {

        DailyRowLayoutBinding mBinding;

        public TrendsViewHolder(DailyRowLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void update(DailyItem dailyItem) {
            mBinding.setDailyItem(dailyItem);
            mBinding.executePendingBindings();
        }
    }
}
