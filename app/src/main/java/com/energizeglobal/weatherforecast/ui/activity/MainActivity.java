package com.energizeglobal.weatherforecast.ui.activity;

import android.os.Bundle;

import com.energizeglobal.weatherforecast.WeatherForecastApplication;
import com.energizeglobal.weatherforecast.R;
import com.energizeglobal.weatherforecast.api.WeatherApi;
import com.energizeglobal.weatherforecast.databinding.ActivityMainBinding;
import com.energizeglobal.weatherforecast.model.CurrentWeatherResponse;
import com.energizeglobal.weatherforecast.model.DailyForecastResponse;
import com.energizeglobal.weatherforecast.ui.DailyForecastAdapter;
import com.energizeglobal.weatherforecast.viewModel.MainActivityView;
import com.energizeglobal.weatherforecast.viewModel.MainViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainActivityView {

    @Inject
    WeatherApi mWeatherApi;

    private DailyForecastAdapter mDailyForecastAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeatherForecastApplication.getComponent().inject(this);

        mViewModel = new MainViewModel(mWeatherApi);
        mViewModel.attach(this);

        bindView(R.layout.activity_main);
        setActionBar(mBinding.toolbar);
        mBinding.setIsLoading(true);

        mDailyForecastAdapter = new DailyForecastAdapter();
        mBinding.dailyItems.setAdapter(mDailyForecastAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.getWeatherData();
    }

    @Override
    public void loadDailyForecast(DailyForecastResponse dailyForecastResponse) {
        if (dailyForecastResponse.getDailyItems() != null) {
            mDailyForecastAdapter.setTempItems(dailyForecastResponse.getDailyItems());
            mBinding.setIsLoading(false);
            if (dailyForecastResponse.getDailyItems().size() > 0) {
                mBinding.setDay(dailyForecastResponse.getDailyItems().get(0));
            }
        }
    }

    @Override
    public void loadCurrentWeather(CurrentWeatherResponse currentWeatherResponse) {
        mBinding.setIsLoading(false);
        if (currentWeatherResponse.getWeatherIcon() <= 5) {
            mBinding.weatherInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sun, 0);
        } else if ((currentWeatherResponse.getWeatherIcon() > 5 && currentWeatherResponse.getWeatherIcon() <= 11)
                || (currentWeatherResponse.getWeatherIcon() > 37 && currentWeatherResponse.getWeatherIcon() <= 42)) {
            mBinding.weatherInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cloud, 0);
        } else if (currentWeatherResponse.getWeatherIcon() > 12 && currentWeatherResponse.getWeatherIcon() <= 29) {
            mBinding.weatherInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rain, 0);
        } else if (currentWeatherResponse.getWeatherIcon() > 33 && currentWeatherResponse.getWeatherIcon() <= 37) {
            mBinding.weatherInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.moon, 0);
        } else if (currentWeatherResponse.getWeatherIcon() == 43 && currentWeatherResponse.getWeatherIcon() == 44) {
            mBinding.weatherInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.snow, 0);
        }
        mBinding.setCurrentWeather(currentWeatherResponse);
    }
}
