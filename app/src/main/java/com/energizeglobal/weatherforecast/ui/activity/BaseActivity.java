package com.energizeglobal.weatherforecast.ui.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;

import com.energizeglobal.weatherforecast.viewModel.BaseViewModel;
import com.energizeglobal.weatherforecast.viewModel.IView;

public abstract class BaseActivity<B extends ViewDataBinding, T extends BaseViewModel> extends AppCompatActivity implements IView {

    protected T mViewModel;
    B mBinding;

    protected final void bindView(int layout) {
        if (mViewModel == null) {
            throw new IllegalStateException("viewModel must not be null and should be injected via activityComponent().inject(this)");
        }

        mBinding = DataBindingUtil.setContentView(this, layout);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mViewModel.clearSubscrition();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.detach();
    }

    @Override
    public void error(Throwable e) {
    }
}
