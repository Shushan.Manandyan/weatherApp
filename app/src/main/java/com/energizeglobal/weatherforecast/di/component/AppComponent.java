package com.energizeglobal.weatherforecast.di.component;

import com.energizeglobal.weatherforecast.di.module.AppModule;
import com.energizeglobal.weatherforecast.di.module.NetworkModule;
import com.energizeglobal.weatherforecast.di.module.WeatherModule;
import com.energizeglobal.weatherforecast.ui.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, WeatherModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
}
