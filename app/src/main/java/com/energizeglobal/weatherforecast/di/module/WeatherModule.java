package com.energizeglobal.weatherforecast.di.module;

import com.energizeglobal.weatherforecast.api.WeatherApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class WeatherModule {

    @Provides
    @Singleton
    WeatherApi provideRetrofit(Retrofit retrofit) {
        return retrofit.create(WeatherApi.class);
    }

}
