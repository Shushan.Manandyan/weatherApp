package com.energizeglobal.weatherforecast.util;

import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Converter {

    public static final String DATE_TO_TIME_CONVERTER= "h:mm a";
    public static final String DATE_TO_WEEK_DAY_CONVERTER= "EEEE";

    public static String convertDateToTime(long date){
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TO_TIME_CONVERTER);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date*1000);
        return formatter.format(calendar.getTime());

    }

    public static String convertDateToWeekDay(long date){
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TO_WEEK_DAY_CONVERTER);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date*1000);
        return formatter.format(calendar.getTime());
    }
}
