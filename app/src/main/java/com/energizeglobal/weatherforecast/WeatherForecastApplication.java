package com.energizeglobal.weatherforecast;

import android.app.Application;

import com.energizeglobal.weatherforecast.di.component.AppComponent;
import com.energizeglobal.weatherforecast.di.component.DaggerAppComponent;
import com.energizeglobal.weatherforecast.di.module.AppModule;
import com.energizeglobal.weatherforecast.di.module.NetworkModule;
import com.energizeglobal.weatherforecast.di.module.WeatherModule;

import io.realm.Realm;

public class WeatherForecastApplication extends Application {

    private static AppComponent sComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        sComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .weatherModule(new WeatherModule())
                .build();
    }

    public static AppComponent getComponent() {
        return sComponent;
    }
}
